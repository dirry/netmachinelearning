using System.Collections.Generic;
using System.Linq;

namespace SpamOrHam
{
    internal static class StatisticsExtensions
    {
        internal static double Probability(this Statistics statistics, SpamOrHam type, string token)
        {
            var pTokenInType = statistics.Probability(token.ToLower(), type);
            var pToken = statistics.Probability(token.ToLower());
            var pType = statistics.Probability(type);

            return pTokenInType * pToken / pType;
        }
        
        internal static double Probability(this Statistics statistics, string token)
        {
            var statsOfToken = statistics[token.ToLower()];
            var numberOfMatchingTokens = statsOfToken.Count;

            var numberOfTokens = statistics.TokenCount;

            var probability = (double)numberOfMatchingTokens/numberOfTokens;

            return probability;
        }
        
        internal static double Probability(this Statistics statistics, SpamOrHam type)
        {
            return type == SpamOrHam.Spam
                ? (double) statistics.SpamCount/statistics.TokenCount
                : (double) statistics.HamCount/statistics.TokenCount;
        }
        
        internal static double Probability(this Statistics statistics, string token, SpamOrHam type)
        {
            var tokenStats = statistics[token.ToLower()];

            return type == SpamOrHam.Spam
                ? (double) tokenStats.SpamCount/tokenStats.Count
                : (double) tokenStats.HamCount/tokenStats.Count;
        }

        internal static double Score(this Statistics statistics, string message, SpamOrHam type)
        {
            var tokens = message.ToTokens();
            return tokens
                .Where(statistics.Contains)
                .Select(token => statistics.Probability(token, type))
                .Append(statistics.Probability(type))
                .Average();
        }

        private static IEnumerable<T> Append<T>(this IEnumerable<T> enumerable, T append)
        {
            foreach (var item in enumerable)
            {
                yield return item;
            }

            yield return append;
        }
    }
}
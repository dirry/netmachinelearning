using System;
using System.Collections.Generic;
using System.Linq;
using MoreLinq;

namespace ShadesOfGrey
{
    public sealed class BasicClassifier<T> : IClassifier<T>
    {
        public BasicClassifier(IDistanceCalculator<T> distanceCalculator)
        {
            DistanceCalculator = distanceCalculator;
        }

        private IList<Observation<T>> TrainingSet { get; } = new List<Observation<T>>();
        private IDistanceCalculator<T> DistanceCalculator { get; }
        private bool IsCompleted { get; set; }
        
        public string Predict(IEnumerable<T> pixels)
        {
            if (pixels == null) throw new ArgumentNullException(nameof(pixels));
            
            var pixelsList = pixels.ToList();
            
            var bestGuess = TrainingSet.AsParallel().Select(training =>
            {
                var distance = DistanceCalculator.Calculate(training.Pixels.ToList<T>(), pixelsList);
                return new Prediction(training.Label, distance);
            }).MinBy(prediction => prediction.Distance);

            return bestGuess.Label ?? string.Empty;
        }

        internal struct Prediction
        {
            public Prediction Default => new Prediction(string.Empty, double.MaxValue);

            public string Label { get; }
            public double Distance { get; }

            public Prediction(string label, double distance)
            {
                Label = label;
                Distance = distance;
            }
        }

        public void OnNext(Observation<T> value)
        {
            if (IsCompleted) return;

            if (value == null) throw new ArgumentNullException(nameof(value));

            TrainingSet.Add(value);
        }

        public void OnError(Exception error)
        {
            IsCompleted = true;
        }

        public void OnCompleted()
        {
            IsCompleted = true;
        }
    }
}
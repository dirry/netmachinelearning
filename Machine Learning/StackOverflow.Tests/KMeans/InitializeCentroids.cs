using System;
using System.Linq;
using FluentAssertions;
using Microsoft.FSharp.Core;
using Xunit;

namespace StackOverflow.Tests.KMeans
{
    public sealed class InitializeCentroids
    {
        private static double[][] Observations => new[]
        {
            new[] { 05d, 06d, 07d, 08d, 09d, 10d },
            new[] { 08d, 09d, 10d, 11d, 12d, 13d },
            new[] { 13d, 14d, 15d, 16d, 17d, 18d },
            new[] { 21d, 22d, 23d, 24d, 25d, 26d },
            new[] { 34d, 35d, 36d, 37d, 38d, 39d }, 
            new[] { 55d, 56d, 57d, 58d, 59d, 60d }, 
            new[] { 89d, 90d, 91d, 92d, 93d, 94d } 
        };

        private static FSharpFunc<int, FSharpFunc<int, int[]>> Picker =>
            FSharpFunc<int, FSharpFunc<int, int[]>>.FromConverter(size => FSharpFunc<int, int[]>.FromConverter(k => GetPicker(size, k)));

        private static int[] GetPicker(int size, int k)
        {
            k.Should().BeLessOrEqualTo(size);

            return Enumerable.Range(0, k).ToArray();
        }

        [Fact]
        public void ShouldNotThrow()
        {
            Action result = () => StackOverflow.KMeans.initializeCentroids(Observations, 5, Picker);
            result.ShouldNotThrow();
        }

        [Fact]
        public void ShouldThrow_WhenPickerIsNull()
        {
            Action result = () => StackOverflow.KMeans.initializeCentroids(Observations, 5, null);
            result.ShouldThrow<NullReferenceException>();
        }

        [Fact]
        public void ShouldThrow_WhenObservationsAreNull()
        {
            Action result = () => StackOverflow.KMeans.initializeCentroids(null, 5, Picker);
            result.ShouldThrow<ArgumentNullException>();
        }
        
        [Fact]
        public void ShouldReturnCentroids()
        {
            var centroids = StackOverflow.KMeans.initializeCentroids(Observations, 5, Picker);
            
            centroids.Should().NotBeNull();
            centroids.Length.Should().Be(5);

            centroids[0].Item1.Should().Be(1);
            centroids[1].Item1.Should().Be(2);
            centroids[2].Item1.Should().Be(3);
            centroids[3].Item1.Should().Be(4);
            centroids[4].Item1.Should().Be(5);

            centroids[0].Item2.Length.Should().Be(6);
            centroids[0].Item2[0].Should().Be(05d);
            centroids[0].Item2[1].Should().Be(06d);
            centroids[0].Item2[2].Should().Be(07d);
            centroids[0].Item2[3].Should().Be(08d);
            centroids[0].Item2[4].Should().Be(09d);
            centroids[0].Item2[5].Should().Be(10d);
        }
    }
}
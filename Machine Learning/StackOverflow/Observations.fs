﻿namespace StackOverflow

open System
open System.IO

module Observations = 
    type Observation = float[]
    type Observations = Observation seq
    type Centroid = Observation

    let folder = "."
    let file = "userprofiles-toptags.csv"

    let headers, observations = 
        let raw = (folder, file) |> Path.Combine |> File.ReadAllLines

        let headers = (raw.[0].Split ',').[1..]

        let observations = 
            raw.[1..] 
            |> Array.map (fun line -> (line.Split ',').[1..]) 
            |> Array.map (Array.map float)

        headers, observations

    let rowNormalizer (obs:Observation) : Observation = 
        let max = obs |> Seq.max 
        obs |> Array.map (fun tagUse -> tagUse / max)

    let features = headers.Length
    let distance (obs1:Observation) (obs2:Observation) : float = 
        (obs1, obs2) 
        ||> Seq.map2 (fun u1 u2 -> pown (u1-u2) 2) 
        |> Seq.sum

    let centroidOfFactory (features:int) : (Observations -> Centroid) = 
        if features < 1 then raise (System.ArgumentOutOfRangeException("features", features, "Must be greater than 0."))

        let centroidOf (cluster:Observations) : Centroid = 
            Array.init features (fun index -> cluster |> Seq.averageBy (fun user -> user.[index]))
        centroidOf

    let centroidOf (cluster:Observations) : Centroid = 
        (centroidOfFactory features) cluster
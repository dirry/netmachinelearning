﻿namespace StackOverflow
module InformationCriterions = 
    type Observation = float[]
    type Observations = Observation[]
    type Centroids = seq<Observation>
    type InformationCriterion = Observation[] -> Centroids -> float

    ///<summary>
    ///Rule-Of-Thumb Information Criterion
    ///</summary>
    ///<remarks><code>let k_ruleOfThumb = ruleOfThumb (observations.Length)</code></remarks>
    let ruleOfThumb (n:int) = sqrt (float n/2.)
    
    let squareError (obs1:Observation) (obs2:Observation) = 
        (obs1, obs2)
        ||> Seq.zip
        |> Seq.sumBy (fun (x1,x2) -> pown (x1-x2) 2)

    ///<summary>
    ///Residual Sum of Squares 
    ///</summary>
    let RSS (dataset:Observations) (centroids:Centroids) = 
        dataset
        |> Seq.sumBy (fun obs -> 
            centroids 
            |> Seq.map (squareError obs)
            |> Seq.min)

    ///<summary>
    /// Akaike Information Criterion
    ///</summary>
    let AIC (dataset:Observations) (centroids:Centroids) : float = 
        let k = centroids |> Seq.length
        let m = dataset.[0] |> Seq.length
        RSS dataset centroids + (float (2*m*k))
using System.Collections.Generic;

namespace ShadesOfGrey
{
    public interface IDistanceCalculator<T>
    {
        double Calculate(IList<T> left, IList<T> right);
    }
}
﻿open FSharp.Data 
open FSharp.Charting

type Data = CsvProvider<"day.csv">
type Obs = Data.Row
type Model = Obs -> float

let window (n:int) (series:float seq) = 
    series 
    |> Seq.windowed n
    |> Seq.map (fun e -> e |> Seq.average)
    |> Seq.toList

let model (θ0:float, θ1:float) (obs:Obs) = 
    let t = (float obs.Instant)
    θ0 + θ1 * t

let cost (data:Obs seq) (m:Model) = 
    data
    |> Seq.sumBy (fun x -> (float x.Cnt - m x) ** 2.)
    |> sqrt

let update (α:float) (θ0:float, θ1:float) (obs:Obs) = 
    let x = float obs.Cnt
    let t = float obs.Instant
    
    // error ε between number of bicycles x
    // and the value predicted by the linear model (θ0 + θ1 * t)
    let ε = x - (θ0 + θ1 * t)

    // calculated by differention of [x - (θ₀ + θ₁t)]² by θ₀
    let δθ0 = 2. * ε 
    let θ0' = θ0 + α * δθ0

    // calculated by differention of [x - (θ₀ + θ₁t)]² by θ₁ 
    let δΘ1 = 2. * t * ε 
    let θ1' = θ1 + α * δΘ1
    
    θ0', θ1'

let showError (α:float) (data: Obs seq) = 
    let hiRate = 10. * α 
    data
        |> Seq.scan (fun (θ0,θ1) obs -> update hiRate (θ0,θ1) obs) (0., 0.)
        |> Seq.map (model >> cost data)
        |> Chart.Line
        |> Chart.WithTitle "Error Rate"
        |> Chart.Show

let batchUpdate (α:float) (θ0:float, θ1:float) (data:Obs seq) = 
    let updates = data
                  |> Seq.map (update α (θ0, θ1))
    let θ0' = updates |> Seq.averageBy fst
    let θ1' = updates |> Seq.averageBy snd
    θ0', θ1'

let batch (α:float) (iterations:int) (data: Obs seq) = 
    let rec search (θ0, θ1) i = 
        if (i = 0) 
        then (θ0, θ1)
        else search (batchUpdate α (θ0, θ1) data) (i-1)
    search (0. ,0.) iterations

let showBatchedError (α:float) (data: Obs seq) = 
    Seq.unfold (fun (θ0, θ1) -> 
        let (θ0', θ1') = batchUpdate α (θ0, θ1) data
        let ε = model (θ0, θ1) |> cost data
        Some (ε, (θ0', θ1'))) (0., 0.)
    |> Seq.take  (Seq.length data)
    |> Seq.toList 
    |> Chart.Line
    |> Chart.WithTitle "Batched Error Rate"
    |> Chart.Show

let stochastic rate (theta0, theta1) (data:seq<Obs>) =
    data
    |> Seq.fold (fun (t0, t1) obs -> update rate (t0, t1) obs) (theta0, theta1)

[<EntryPoint>]
let main argv = 
    let dataset = Data.Load("day.csv")
    let data = dataset.Rows
    let counts = [ for observation in data -> (float)observation.Cnt ]
    
    let average = counts |> Seq.average
    let averageError = counts |> Seq.averageBy (fun v -> abs (v - average)) 

    let model0 = model (average, 0.)
    let model1 = model (6000., -4.5)
    
    let rate = 0.1 ** 8.
    let model2 = model (stochastic rate (0., 0.) data)

    showBatchedError (10. ** -6.) data
    showError rate data
    
    average  |> printfn "Average rents: %f"
    averageError |> printfn "Average error: %f"

    cost data model0 |> printfn "Cost model0: %.0f"
    cost data model1 |> printfn "Cost model1: %.0f"

    Chart.Combine[
        counts |> Chart.Line
        window 30 counts |> Chart.Line
        [for obs in data -> model0 obs]  |> Chart.Line
        [for obs in data -> model1 obs]  |> Chart.Line
        [for obs in data -> model2 obs]  |> Chart.Line

        [for obs in data -> average+averageError]  |> Chart.Line
        [for obs in data -> average-averageError]  |> Chart.Line
    ] |> Chart.Show
    0 

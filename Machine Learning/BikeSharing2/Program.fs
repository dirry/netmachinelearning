﻿System.Environment.CurrentDirectory <- __SOURCE_DIRECTORY__

open FSharp.Data 
open FSharp.Charting
open MathNet
open MathNet.Numerics
open MathNet.Numerics.LinearAlgebra
open MathNet.Numerics.LinearAlgebra.Double
open MathNet.Numerics.Providers.LinearAlgebra.Mkl

Control.LinearAlgebraProvider <- MklLinearAlgebraProvider ()

type Data = CsvProvider<"day.csv">
type Obs = Data.Row
type Model = Obs -> float
type Featurizer = Obs -> float list
type Vec = Vector<float>
type Mat = Matrix<float>

let dataset = Data.Load "day.csv"
let data = dataset.Rows

let cost (θ:Vec) (Y:Vec) (X:Mat) = 
    let θ' = Y - (θ * X.Transpose ())
    (θ' * θ') |> sqrt

let predict (θ:Vec) (v:Vec) = θ * v

let estimate (Y:Vec) (X:Mat) = 
    (X.Transpose() * X).Inverse() * X.Transpose() * Y

/// <summary>Fisher-Yates shuffle</summary>
let shuffle (arr:'a[]) = 
    let seed = 314159
    let rng = System.Random seed
    let arr = Array.copy arr
    let l = arr.Length
    for i in (l-1)..(-1)..1 do
        let temp = arr.[i]
        let j = rng.Next (0, i+1)
        arr.[i] <- arr.[j]
        arr.[j] <- temp
    arr

let (training, validation) = 
    let shuffled = 
        data 
        |> Seq.toArray
        |> shuffle
    let size = 0.7 * (float (Array.length shuffled)) |> int
    shuffled.[..size], shuffled.[size+1..]

let predictor (f:Featurizer) (θ:Vec) = 
    f >> vector >> (*) θ

let evaluate (model:Model) (data:Obs seq) = 
    data 
    |> Seq.averageBy (fun obs -> abs (model obs - float obs.Cnt))

let model (f:Featurizer) (data:Obs seq) = 
    let Yt, Xt = data
                 |> Seq.toList
                 |> List.map (fun obs -> float obs.Cnt, f obs)
                 |> List.unzip
    let θ = estimate (vector Yt) (matrix Xt)
    let predict = predictor f θ
    θ, predict

[<EntryPoint>]
let main argv = 
    
    let X = matrix [ for obs in data -> [ 1.; float obs.Instant ]]
    let Y = vector [ for obs in data -> float obs.Cnt ]
    let θ = vector [ 6000.
                     -4.5 ]
    
    predict θ (X.Row 0) |> printfn "Prediction: %f"
    cost θ Y X |> printfn "Cost: %f"

    // --------------------

    let featurizer0 (obs:Obs) = [ 1.; float obs.Instant ]
    let (θ0, model0) = model featurizer0 training 

    let featurizer1 (obs:Obs) = [
        1.
        obs.Instant |> float
        obs.Atemp |> float
        obs.Hum |> float
        obs.Temp |> float
        obs.Windspeed |> float
    ]
    let (θ1, model1) = model featurizer1 training

    let featurizer2 (obs:Obs) = [
        1.
        obs.Instant |> float
        obs.Hum |> float
        obs.Temp |> float
        obs.Windspeed |> float
        (if obs.Weekday = 0 then 1. else 0.01) 
        (if obs.Weekday = 1 then 1. else 0.01)
        (if obs.Weekday = 2 then 1. else 0.01) 
        (if obs.Weekday = 3 then 1. else 0.01)
        (if obs.Weekday = 4 then 1. else 0.01) 
        (if obs.Weekday = 5 then 1. else 0.01)
        (if obs.Weekday = 6 then 1. else 0.01) 
    ]
    let (θ2, model2) = model featurizer2 training

    printfn "Model 0"
    evaluate model0 training |> printfn "\tTraining: %.0f"
    evaluate model0 validation |> printfn "\tValidation: %.0f"

    printfn "Model 1"
    evaluate model1 training |> printfn "\tTraining: %.0f"
    evaluate model1 validation |> printfn "\tValidation: %.0f"

    printfn "Model 2"
    evaluate model2 training |> printfn "\tTraining: %.0f"
    evaluate model2 validation |> printfn "\tValidation: %.0f"

    [for obs in data -> (float)obs.Temp * 41., obs.Cnt] 
    |> Chart.Point |> Chart.Show

    Chart.Combine [
        [for obs in data -> float obs.Cnt ] |> (fun n -> Chart.Line (n, Name="Data"))
        [for obs in data -> model0 obs ] |> (fun n -> Chart.Line (n, Name="Model 0"))
        [for obs in data -> model1 obs ] |> (fun n -> Chart.Line (n, Name="Model 1"))
        [for obs in data -> model2 obs ] |> (fun n -> Chart.Line (n, Name="Model 2"))
    ] |> Chart.Show

    Chart.Combine [
        [ for obs in data -> float obs.Cnt, model1 obs ] |> Chart.Point
        [ for obs in data -> float obs.Cnt, model2 obs ] |> Chart.Point
    ]  |> Chart.Show

    0

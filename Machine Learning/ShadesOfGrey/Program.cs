﻿using System;
using System.IO;
using System.Reactive;
using System.Reactive.Linq;
using System.Reflection;
using System.Threading.Tasks;
using DryIoc;

namespace ShadesOfGrey
{
    internal class Program
    {
        internal static void Main()
        {
            var resolver = ResolverFactory.Create();

            var dataReader = resolver.Resolve<IDataReader<byte>>();
            var classifier = resolver.Resolve<IClassifier<byte>>();
            
            var trainingsSampleFileInfo = GetTrainingsSampleFileInfo();
            
            Console.WriteLine("Training...");

            var obs = Observer.Create<Observation<byte>>(classifier.OnNext, classifier.OnError, async() =>
            {
                classifier.OnCompleted();

                Console.WriteLine("Training completed. Validating Classifier.");

                var score = await ValidateClassifierAsync(classifier, dataReader);
                Console.WriteLine("Score: {0:P2}", score);
            });
            
            using (dataReader.ReadObservations(trainingsSampleFileInfo).Subscribe(obs))
                Console.ReadLine();
        }

        private static async Task<double> ValidateClassifierAsync(IClassifier<byte> classifier, IDataReader<byte> dataReader)
        {
            var validationSampleFileInfo = GetValidationSampleFileInfo();
            
            var score = await dataReader.ReadObservations(validationSampleFileInfo)
                .Select(sample =>
                {
                    var prediction = classifier.Predict(sample.Pixels);
                    return prediction == sample.Label ? 1.0d : 0.0d;
                }).Average().LastAsync();

            return score;
        }
        
        private static FileInfo GetValidationSampleFileInfo()
        {
            const string validationSampleFileName = "validationsample.csv";
            return new FileInfo(Path.Combine(GetDataDirectory(), validationSampleFileName));
        }

        private static FileInfo GetTrainingsSampleFileInfo()
        {
            const string trainingsSampleFileName = "trainingsample.csv";
            return new FileInfo(Path.Combine(GetDataDirectory(), trainingsSampleFileName));
        }

        private static string GetDataDirectory()
        {
            var appPath = Assembly.GetExecutingAssembly().Location;
            var appDirectory = Path.GetDirectoryName(appPath) ?? string.Empty;
            return Path.Combine(appDirectory, "Data");
        }
    }
}

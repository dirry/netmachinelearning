using System;
using System.Collections.Generic;

namespace ShadesOfGrey
{
    public interface IClassifier<T> : IObserver<Observation<T>>
    {
        string Predict(IEnumerable<T> pixels);
    }
}
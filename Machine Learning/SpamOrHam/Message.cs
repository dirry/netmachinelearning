namespace SpamOrHam
{
    public struct Message
    {
        public SpamOrHam Type { get; }
        public string Content { get; }

        public Message(SpamOrHam type, string content)
        {
            Type = type;
            Content = content;
        }

        public override string ToString()
        {
            return string.Format($"\t{Type}\t{Content}");
        }
    }
}
using System;
using System.Linq;
using System.Reactive.Linq;

namespace SpamOrHam
{
    public static class ObservableExtensions
    {
        public static IObservable<T> Copy<T>(this IObservable<T> observable)
        {
            return observable.ToEnumerable().ToList().ToObservable();
        }

        public static IObservable<T> Split<T>(this IObservable<T> source, out IObservable<T> testData, int take = 3)
        {
            testData = SplitInternally(source, take, false);
            return SplitInternally(source, take, true);
        }

        private static IObservable<T> SplitInternally<T>(IObservable<T> source, int take, bool isLearnData)
        {
            return Observable.Create<T>(observer =>
            {
                var countLock = new object();
                var count = 0;

                return source.Subscribe(item =>
                {
                    var use = isLearnData;
                    lock (countLock)
                    {
                        if (count == take)
                        {
                            count = 0;
                            use = !isLearnData;
                        }
                        else
                        {
                            count++;
                        }
                    }

                    if (use)
                    {
                        observer.OnNext(item);
                    }

                },observer.OnError, observer.OnCompleted);
            });
        }
    }
}
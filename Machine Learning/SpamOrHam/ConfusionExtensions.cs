using System;

namespace SpamOrHam
{
    public static class ConfusionExtensions
    {
        public static double Accuracy (this Confusion c)
        {
            return (c.TruePositive + c.TrueNegative) / c.TotalPopulation;
        }

        public static double Prevalence (this Confusion c)
        {
            return c.ConditionPositive / c.TotalPopulation;
        }

        public static double PositivePredictiveValue (this Confusion c)
        {
            return c.TruePositive / c.TestOutcomePositive;
        }

        public static double Precision (this Confusion c)
        {
            return c.PositivePredictiveValue ();
        }

        public static double FalseOmissionRate (this Confusion c)
        {
            return c.FalseNegative / c.TestOutcomeNegative;
        }

        public static double PositiveLikelihoodRatio (this Confusion c)
        {
            return c.TruePositiveRate () / c.FalsePositiveRate ();
        }

        public static double NegativeLikelihoodRatio (this Confusion c)
        {
            return c.FalseNegativeRate () / c.TrueNegativeRate ();
        }

        public static double FalseDiscoveryRate (this Confusion c)
        {
            return c.FalsePositive / c.TestOutcomePositive;
        }

        public static double NegativePredictiveValue (this Confusion c)
        {
            return c.TrueNegative / c.TestOutcomeNegative;
        }

        public static double DiagnosticOddsRatio (this Confusion c)
        {
            return c.PositiveLikelihoodRatio () / c.NegativeLikelihoodRatio ();
        }

        public static double TruePositiveRate (this Confusion c)
        {
            return c.TruePositive / c.ConditionPositive;
        }

        public static double Recall (this Confusion c)
        {
            return c.TruePositiveRate ();
        }

        public static double Sensitivity (this Confusion c)
        {
            return c.TruePositiveRate ();
        }

        public static double FalsePositiveRate (this Confusion c)
        {
            return c.FalsePositive / c.ConditionNegative;
        }

        public static double FallOut (this Confusion c)
        {
            return c.FalsePositiveRate ();
        }

        public static double FalseNegativeRate (this Confusion c)
        {
            return c.FalseNegative / c.ConditionPositive;
        }

        public static double MissRate (this Confusion c)
        {
            return c.FalseNegativeRate ();
        }

        public static double TrueNegativeRate (this Confusion c)
        {
            return c.TrueNegative / c.ConditionNegative;
        }

        public static double Specificity (this Confusion c)
        {
            return c.TrueNegativeRate ();
        }



        public static double FMeasure (this Confusion c, double beta = 1)
        {
            return Math.Pow (beta, 2) * c.Precision () * c.Recall () /
                   (Math.Pow (beta, 2) * c.Precision () + c.Recall ());
        }


    }
}
﻿namespace StackOverflow
module KMeans = 
    type Observation = float[]
    type Observations = Observation[]
    type Cluster = int * Observation
    type DistanceFunction = Observation -> Observation -> float
    type Classifier = Observation -> Cluster[] -> DistanceFunction -> int
    type Centroids = seq<Observation>
    type CentroidOfFunction = Centroids -> float[]
    type Clustering = Observation[] -> int -> (Cluster[] * Classifier)
    type Picker = int -> int -> int[]
    type ClustersChanged = Cluster[] -> Cluster[] -> bool
    type Clusterizer = DistanceFunction -> CentroidOfFunction -> Picker -> ClustersChanged -> Clustering
    type InitializedValues = Observations -> int -> (Cluster[] * Cluster[])
    
    let pickFrom (size:int) (k:int) : int[] = 
        if size <= 0 then raise (System.ArgumentOutOfRangeException("size"))
        if k <= 0 then raise (System.ArgumentOutOfRangeException("k"))
        if k > size then raise (System.ArgumentOutOfRangeException("k"))
    
        let random = System.Random ()
        let rec pick (set:int Set) = 
            let canditate = random.Next size
            let set = set.Add canditate
            if set.Count = k 
            then set 
            else pick set
        pick Set.empty |> Set.toArray

    let initializeAssignments (observations:Observations) : (Cluster[]) = 
        if observations = null then raise (System.ArgumentNullException ("observations"))
        
        let assignments = observations 
                          |> Array.map (fun x -> 0, x)
        assignments

    let initializeCentroids (observations:Observations) (k:int) (pickFrom: Picker) : (Cluster[]) = 
        if observations = null then raise (System.ArgumentNullException ("observations"))
        
        let size = Array.length observations
        let centroids = pickFrom size k 
                        |> Array.mapi (fun i index -> i+1, observations.[index])
        centroids

    let hasChanged (clusters:Cluster[]) (clusters':Cluster[]) : bool = 
        if clusters = null then raise (System.ArgumentNullException("clusters"))
        if clusters' = null then raise (System.ArgumentNullException("clusters'"))
        if clusters.Length <> clusters'.Length then raise (System.InvalidOperationException("Clusters must be of equal length."))

        (clusters, clusters') 
            ||> Seq.zip
            |> Seq.exists (fun ((oldClusterID, _),(newClusterID, _)) -> not (oldClusterID = newClusterID))

    let classifier (observation:Observation) (centroids:Cluster[]) (distance:DistanceFunction):int = 
        if observation = null then raise (System.ArgumentNullException("observation"))
        if centroids = null  then raise (System.ArgumentNullException("centroids"))
        if centroids.Length = 0 then raise (System.ArgumentException("is empty.", "centroids"))
        centroids 
        |> Array.minBy (fun (_, centroid) -> distance observation centroid) 
        |> fst

    let clusterize (distance:DistanceFunction) (centroidOf:CentroidOfFunction) (pickFrom: Picker) (hasChanged: ClustersChanged) (observations:Observations) (k:int) = 
        
        let rec search (assignments : Cluster[]) (centroids : Cluster[]) : (Cluster[] * Classifier) = 
            
            let assignments' = assignments 
                               |> Array.map (fun (_, observation) -> 
                                    let closestControidId = classifier observation centroids distance
                                    (closestControidId, observation))

            if hasChanged assignments assignments'
            then
                let centroids' = assignments'
                                 |> Seq.groupBy fst
                                 |> Seq.map (fun (clusterID, group) -> clusterID, group |> Seq.map snd |> centroidOf)
                                 |> Seq.toArray
                search centroids' assignments'
            else centroids, classifier
        
        let centroids = initializeCentroids observations k pickFrom
        let assignments = initializeAssignments observations
        search centroids assignments
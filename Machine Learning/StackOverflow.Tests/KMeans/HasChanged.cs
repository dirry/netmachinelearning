﻿using System;
using FluentAssertions;
using Xunit;

namespace StackOverflow.Tests.KMeans
{
    public sealed class HasChanged
    {
        [Fact]
        public void ShouldNotThrow()
        {
            var observation = new[] {0d};
            var cluster = new Tuple<int, double[]>(0, observation);
            var clusters = new[] {cluster};
            
            Action act = () => StackOverflow.KMeans.hasChanged(clusters, clusters);
            act.ShouldNotThrow();
        }
        
        [Theory]
        [InlineData(0,0,0,false)]
        [InlineData(0,0,1,true)]
        [InlineData(0,1,2,true)]
        public void ShouldReturnTrue_WhenTheClustersAreNotIdentical(int id1, int id2, int id3, bool expected)
        {
            var observation = new[] {0d};

            var cluster1 = new Tuple<int, double[]>(id1, observation);
            var cluster2 = new Tuple<int, double[]>(id2, observation);
            var cluster3 = new Tuple<int, double[]>(id3, observation);

            var clusters1 = new[] {cluster1, cluster2};
            var clusters2 = new[] {cluster1, cluster3};

            var result = StackOverflow.KMeans.hasChanged(clusters1, clusters2);
            result.Should().Be(expected);
        }

        [Fact]
        public void SchouldThrowArgumentNullExceptionIfFirstParameterIsNull()
        {
            var clusters = new Tuple<int, double[]>[] {};
            Action act = () => StackOverflow.KMeans.hasChanged(null, clusters);
            act.ShouldThrow<ArgumentNullException>();
        }

        [Fact]
        public void SchouldThrowArgumentNullExceptionIfSecondParameterIsNull()
        {
            var clusters = new Tuple<int, double[]>[] {};
            Action act = () => StackOverflow.KMeans.hasChanged(clusters, null);
            act.ShouldThrow<ArgumentNullException>();
        }

        [Fact]
        public void SchouldThrowInvalidOperatinExceptionIfLengthsOfParametersAreDifferent()
        {
            var observation = new[] {0d};

            var cluster1 = new Tuple<int, double[]>(0, observation);
            var cluster2 = new Tuple<int, double[]>(0, observation);

            var clusters1 = new[] {cluster1, cluster2};
            var clusters2 = new[] {cluster1};

            Action act = () => StackOverflow.KMeans.hasChanged(clusters1, clusters2);

            act.ShouldThrow<InvalidOperationException>();
        }
    }
}

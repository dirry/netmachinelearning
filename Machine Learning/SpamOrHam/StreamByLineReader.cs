using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Disposables;
using MoreLinq;
using System.Threading.Tasks;

namespace SpamOrHam
{
    internal sealed class StreamByLineReader : IStreamByLineReader
    {
        private IList<IObserver<string>> Observers { get; } = new List<IObserver<string>>();

        public IDisposable Subscribe(IObserver<string> observer)
        {
            Observers.Add(observer);
            return Disposable.Create(() => Observers.Remove(observer));
        }

        public async Task RunAsync(Func<Stream> streamFactory)
        {
            try
            {
                using(var stream = streamFactory())
                using(var reader = new StreamReader(stream))
                {
                    while(!reader.EndOfStream)
                    {
                        var line = await reader.ReadLineAsync();
                        
                        Observers.ToArray().ForEach(o => o.OnNext(line));
                    }
                }

                Observers.ToArray().ForEach(o => o.OnCompleted());
            }
            catch(Exception e)
            {
                Observers.ToArray().ForEach(o => o.OnError(e));
            }
        }
    }
}
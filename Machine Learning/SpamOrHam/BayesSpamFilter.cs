﻿namespace SpamOrHam
{
    public sealed class BayesSpamFilter: ISpamFilter
    {
        private Statistics Statistics { get; }
        private double Beta { get; }

        public Confusion Confusion { get; }

        internal BayesSpamFilter( Statistics statistics, double beta, Confusion confusion )
        {
            Statistics = statistics;
            Beta = beta;
            Confusion = confusion;
        }

        public bool IsPropablySpam(string message)
        {
            var propability = Statistics.Score(message, SpamOrHam.Spam);

            return propability > Beta;
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;

namespace ShadesOfGrey
{
    public sealed class EuclideanDistance : IDistanceCalculator<byte>
    {
        public double Calculate(IList<byte> left, IList<byte> right)
        {
            if (left.Count != right.Count)
                throw new ArgumentException("Count of left collection and right collection do not match.");

            var sum = left.Zip(right, (x, y) => (double)x - y)
                .Select(z => Math.Pow(z,2))
                .Sum();

            return Math.Sqrt(sum);
        }
    }
}
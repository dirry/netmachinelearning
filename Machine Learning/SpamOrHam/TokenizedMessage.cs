namespace SpamOrHam
{
    internal class Token
    {
        public Token(SpamOrHam type, string value)
        {
            Type = type;
            Value = value;
        }

        public SpamOrHam Type { get; }
        public string Value { get; }

        public override string ToString()
        {
            return string.Format($"{Type}");
        }
    }
}
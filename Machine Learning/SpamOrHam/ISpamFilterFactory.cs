﻿using System;
using System.Threading.Tasks;

namespace SpamOrHam
{
    public interface ISpamFilterFactory
    {
        Task<ISpamFilter> CreateAsync(IObservable<Message> messages, Func<Task> readMessages);
    }
}
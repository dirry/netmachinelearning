﻿namespace StackOverflow

open System
open System.IO
open FSharp.Data
open FSharp.Charting
open StackOverflow.KMeans
open StackOverflow.Observations
open StackOverflow.InformationCriterions
open System.Linq

module Charts = 
    type Observation = float[]
    type Centroid = Observation
    type Cluster = int * Observation
    type DistanceFunction = Observation -> Observation -> float
    type Classifier = Observation -> Cluster[] -> DistanceFunction -> int
    type Observations = Observation seq
    type CentroidOfFunction = Observations -> Centroid
    type Clustering = Observation[] -> int -> (Cluster[] * Classifier)
    type Picker = int -> int -> int[]
    type ClustersChanged = Cluster[] -> Cluster[] -> bool
    type Clusterizer = DistanceFunction -> CentroidOfFunction -> Picker -> ClustersChanged -> Clustering
    
    // filtered observations
    let observations1 = 
        observations 
        |> Array.map (Array.map float) 
        |> Array.filter (fun x -> Array.sum x > 0.)

    // filtered and normalized observations 
    let observations2 =
        observations1
        |> Array.map rowNormalizer

    let (clusters1 : Cluster[], classifier1 : Classifier) = 
        let clustering = clusterize distance centroidOf pickFrom hasChanged
        let k = 5
        clustering observations1 k

    let (clusters2 : Cluster[], classifier2 : Classifier) =
        let clustering = clusterize distance centroidOf pickFrom hasChanged
        let k = 5
        clustering observations2 k

    //------------------------------------------

    let showOptimalClusterNumber (observations:Observation[]) (clustering:Clusterizer) (distance : DistanceFunction) (centroidOf:CentroidOfFunction) (criterion : InformationCriterion) (pickFrom : Picker) = 
            [1..25].AsParallel()
            |> Seq.map (fun k -> 
                let value = [
                    for _ in 1..10 -> 
                        let (clusters, classifier) = 
                            let clustering = clusterize distance centroidOf pickFrom hasChanged
                            clustering observations k
                        criterion observations (clusters |> Seq.map snd) ] |> List.average 
                k, value)
            |> Chart.Line |> Chart.Show

    let showConsoleOutput (observations:Observation[]) = 
        printfn "%16s %8s %8s %8s" "Tag Name" "Avg" "Min" "Max" 
        headers 
        |> Array.iteri (fun i name -> 
            let col = observations |> Array.map (fun obs -> obs.[i])
    
            let avg = col |> Array.average
            let min = col |> Array.min
            let max = col |> Array.max
    
            printfn "%16s %8f %8f %8f" name avg min max)

    let showClustersOnConsole (clusters:Cluster[]) = 
        clusters 
        |> Seq.iter (fun(id, profile) -> 
            printfn "CLUSTER %i" id
            profile
            |> Array.iteri (fun i value -> printfn "%16s %.1f" headers.[i] value))

    let showChart (headers:string[]) (observations:Observation[]) = 
        let labels = ChartTypes.LabelStyle(Interval=0.25)
        headers 
        |> Seq.mapi(fun i name -> name, observations |> Seq.averageBy(fun obs -> obs.[i]))
        |> Chart.Bar
        |> fun chart -> chart.WithXAxis(LabelStyle=labels) |> Chart.Show

    let showClusters (clusters : Cluster[]) = 
        let labels = ChartTypes.LabelStyle(Interval=0.25)
        Chart.Combine [
            for (id, profile) in clusters -> profile |> Seq.mapi (fun i value -> headers.[i], value) |> Chart.Column
        ] 
        |> fun chart -> chart.WithXAxis(LabelStyle=labels)
        |> fun chart -> chart.WithYAxis(LabelStyle=labels)
        |> Chart.Show
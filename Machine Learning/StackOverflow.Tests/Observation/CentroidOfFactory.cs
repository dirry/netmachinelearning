﻿using System;
using FluentAssertions;
using Xunit;

namespace StackOverflow.Tests.Observation
{
    public sealed class CentroidOfFactory
    {
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(int.MaxValue)]
        public void ShouldNotThrow(int k)
        {
            Action act = () => Observations.centroidOfFactory(k);
            act.ShouldNotThrow();
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(int.MinValue)]
        public void ShouldThrow(int k)
        {
            Action act = () => Observations.centroidOfFactory(k);
            act.ShouldThrow<ArgumentOutOfRangeException>();
        }

        [Fact]
        public void ShouldNotBeNull()
        {
            var result = Observations.centroidOfFactory(2);
            result.Should().NotBeNull();
        }

        [Fact]
        public void Invoke_CentroidOf_ShouldNotThrow()
        {
            var observations = new[]
            {
                new[] {0d, 0d}, 
                new[] {1d, 0d}, 
                new[] {1d, 3d}
            };

            var centroidOf = Observations.centroidOfFactory(2);

            Action act = () => centroidOf.Invoke(observations);
            act.ShouldNotThrow();
        }

        [Fact]
        public void Invoke_CentroidOf_Foo()
        {
            var observations = new[]
            {
                new[] {0d, 0d}, 
                new[] {1d, 0d}, 
                new[] {2d, 6d}
            };

            var centroidOf = Observations.centroidOfFactory(features: 2);

            var result = centroidOf.Invoke(observations);
            result.Length.Should().Be(2);
            result[0].Should().Be(1d);
            result[1].Should().Be(2d);
        }
    }
}

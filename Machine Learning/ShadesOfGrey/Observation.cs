﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace ShadesOfGrey
{
    public sealed class Observation<T> : IEquatable<Observation<T>>
    {
        public Observation(string label, IEnumerable<T> pixels)
        {
            Label = label;
            Pixels = Array.AsReadOnly(pixels.ToArray());
        }

        public string Label { get; }
        public ICollection<T> Pixels { get; }

        #region IEquatable
        private bool Equals(Observation<T> other)
        {
            return string.Equals(Label, other.Label) && Equals(Pixels, other.Pixels);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj is Observation<T> && Equals((Observation<T>) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Label?.GetHashCode() ?? 0)*397) ^ (Pixels?.GetHashCode() ?? 0);
            }
        }

        bool IEquatable<Observation<T>>.Equals(Observation<T> other)
        {
            return Equals(other);
        }
        #endregion

        public override string ToString()
        {
            return Pixels.Aggregate(new StringBuilder().Append(Label), (sb, p) => sb.AppendFormat(",{0}", p), 
                sb => sb.ToString());
        }
    }
}
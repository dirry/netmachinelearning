namespace ShadesOfGrey
{
    public interface IObservationFactory<T>
    {
        Observation<T> Create(string data);
    }
}
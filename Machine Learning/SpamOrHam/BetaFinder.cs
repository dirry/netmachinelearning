﻿using System;
using System.Reactive.Linq;
using System.Threading.Tasks;

namespace SpamOrHam
{
    internal sealed class BetaFinder
    {
        private IObservable<Tuple<double, Message>> RatedMessages { get; }
        private Statistics Statistics { get; }
        private const int SearchSteps = 6;

        internal BetaFinder(IObservable<Message> messages, Statistics statistics)
        {
            Statistics = statistics;
            RatedMessages = ToRatedMessages(messages).Copy();
        }

        public async Task<double> GetBetaAsync()
        {
            var betaRange = await BetaWhereNoHamIsRatedAsSpam();
            var beta = await FindBetaWithMaxAccuracy(betaRange.Item1, betaRange.Item2);

            return beta;
        }

        public async Task<Confusion> GetConfusionAsync()
        {
            return await GetConfusionAsync(await GetBetaAsync());
        }

        private IObservable<Tuple<double, Message>> ToRatedMessages(IObservable<Message> messages)
        {
            return messages
                .Select(message => Tuple.Create(Statistics.Score(message.Content, SpamOrHam.Spam), message))
                .Where(s => Math.Abs(s.Item1) > 0.000001);
        }

        private async Task<Tuple<double, double>> BetaWhereNoHamIsRatedAsSpam()
        {
            var hamMessages = RatedMessages.Where(m => m.Item2.Type == SpamOrHam.Ham);
            var spamMessages = RatedMessages.Where(m => m.Item2.Type == SpamOrHam.Spam);

            var minSpam = await spamMessages.Min(m => m.Item1);
            var maxSpam = await spamMessages.Max(m => m.Item1);
            var maxHam = await hamMessages.Max(m => m.Item1);

            var min = minSpam > maxHam
                ? minSpam
                : maxHam;

            return new Tuple<double, double>(min, maxSpam);
        }

        private async Task<double> FindBetaWithMaxAccuracy(double min, double max, int steps = SearchSteps)
        {
            var middle = min + (max - min) / 2;

            if (steps <= 0)
            {
                return middle;
            }

            return await Task.Run(async () =>
            {
                var leftTask = FindBetaWithMaxAccuracy(min, middle, steps - 1);
                var rightTask = FindBetaWithMaxAccuracy(middle, max, steps - 1);

                var accuracyLeftTask = GetAccuacyAsync(await leftTask);
                var accuracyRightTask = GetAccuacyAsync(await rightTask);

                return await accuracyLeftTask > await accuracyRightTask
                    ? leftTask.Result
                    : rightTask.Result;
            });
        }

        private async Task<double> GetAccuacyAsync(double beta)
        {
            var confusion = await GetConfusionAsync(beta);
            return confusion.Accuracy();
        }

        private async Task<Confusion> GetConfusionAsync(double beta)
        {
            var spamMessages = RatedMessages.Where(p => p.Item1 > beta).Select(r => r.Item2.Type);
            var hamMessages = RatedMessages.Where(p => p.Item1 <= beta).Select(r => r.Item2.Type);

            return await GetConfusionAsync(spamMessages, hamMessages);
        }

        private static async Task<Confusion> GetConfusionAsync(IObservable<SpamOrHam> spamMessages, IObservable<SpamOrHam> hamMessages)
        {
            var spamThatIsReallySpam = spamMessages.Where(m => m == SpamOrHam.Spam);
            var hamThatIsReallySpam = hamMessages.Where(m => m == SpamOrHam.Spam);
            var spamThatIsReallyHam = spamMessages.Where(m => m == SpamOrHam.Ham);
            var hamThatIsReallyHam = hamMessages.Where(m => m == SpamOrHam.Ham);

            var truePositiveCount = await spamThatIsReallySpam.Count();
            var falseNegativeCount = await hamThatIsReallySpam.Count();
            var falsePositiveCount = await spamThatIsReallyHam.Count();
            var trueNegativeCount = await hamThatIsReallyHam.Count();

            return new Confusion(truePositiveCount, falseNegativeCount, falsePositiveCount, trueNegativeCount);
        }
    }
}

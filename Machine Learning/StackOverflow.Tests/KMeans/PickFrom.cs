﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using Xunit;

namespace StackOverflow.Tests.KMeans
{
    using KMeans = StackOverflow.KMeans;

    public sealed class PickFrom
    {
        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(1000)]
        public void ShouldNotThrow_WhenProperNumberIsGiven(int number)
        {
            Action result = () => KMeans.pickFrom(1000, number);
            result.ShouldNotThrow();
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(1000)]
        public void ShouldReturnProperNumberOfElements(int number)
        {
            var result = KMeans.pickFrom(1000, number);
            result.Length.Should().Be(number);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(1000)]
        [InlineData(2000)]
        public void ShouldReturnUniqueResults(int number)
        {
            var result = KMeans.pickFrom(2000, number);
            var numberOfElements = result.Length;
            var numberOfUniqueElements = result.Distinct().Count();
            numberOfElements.Should().Be(numberOfUniqueElements);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-256)]
        public void ShouldThrowArgumentOutOfRangeException_WhenNotEnoughElementsAreRequested(int number)
        {
            var token = new CancellationTokenSource(500).Token;
            Func<int[]> pickFrom = () => KMeans.pickFrom(1000, number);
            Func<Task<int[]>> result = async () => await Task.Run(pickFrom, token);
            result.ShouldThrow<ArgumentOutOfRangeException>();
        }

        [Theory]
        [InlineData(1001)]
        [InlineData(2000)]
        public void ShouldThrowArgumentOutOfRangeException_WhenTooManyElementsAreRequested(int number)
        {
            var token = new CancellationTokenSource(500).Token;
            Func<int[]> pickFrom = () => KMeans.pickFrom(1000, number);
            Func<Task<int[]>> result = async () => await Task.Run(pickFrom, token);
            result.ShouldThrow<ArgumentOutOfRangeException>();
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-1000)]
        public void ShouldThrowArgumentOutOfRangeException_WhenSizeIsToLow(int size)
        {
            var token = new CancellationTokenSource(500).Token;
            Func<int[]> pickFrom = () => KMeans.pickFrom(size, 5);
            Func<Task<int[]>> result = async () => await Task.Run(pickFrom, token);
            result.ShouldThrow<ArgumentOutOfRangeException>();
        }
    }
}

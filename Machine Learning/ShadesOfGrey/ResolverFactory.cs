using DryIoc;

namespace ShadesOfGrey
{
    public static class ResolverFactory
    {
        public static IResolver Create()
        {
            var container = new Container();

            container.Register<IObservationFactory<byte>, ObservationFactory>();
            container.Register<IDataReader<byte>, DataReader<byte>>();
            container.Register<IDistanceCalculator<byte>, EuclideanDistance>();
            container.Register<IClassifier<byte>, BasicClassifier<byte>>();

            return container;
        }
    }
}
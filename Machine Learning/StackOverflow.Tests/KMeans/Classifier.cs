﻿using System;
using FluentAssertions;
using Microsoft.FSharp.Core;
using Xunit;

namespace StackOverflow.Tests.KMeans
{
    public sealed class Classifier
    {
        private static FSharpFunc<double[], FSharpFunc<double[], double>> Distance => 
            FSharpFunc<double[], FSharpFunc<double[], double>>.FromConverter(i => FSharpFunc<double[], double>.FromConverter(j => Observations.distance(i,j)));

        [Fact]
        public void ShouldThrowExceptionIfCentroidsAreEmpty()
        {
            var observation = new[] {0d};
            var centroids = new Tuple<int, double[]>[] {};
            
            Action act = () => StackOverflow.KMeans.classifier(observation, centroids, Distance);
            act.ShouldThrow<ArgumentException>();
        }

        [Fact]
        public void ShouldNotThrow()
        {
            var observation = new[] {0d};
            var centroids = new [] {new Tuple<int, double[]>(0, observation) };
            
            Action act = () => StackOverflow.KMeans.classifier(observation, centroids, Distance);
            act.ShouldNotThrow();
        }

        [Fact]
        public void ShouldThrowArgumentNullExceptionIfObservationsIsNull()
        {
            var observation = new[] {0d};
            var centroids = new [] {new Tuple<int, double[]>(0, observation) };
            
            Action act = () => StackOverflow.KMeans.classifier(null, centroids, Distance);
            act.ShouldThrow<ArgumentNullException>();
        }

        [Fact]
        public void ShouldThrowArgumentNullExceptionIfCentroidsIsNull()
        {
            var observation = new[] {0d};
            
            Action act = () => StackOverflow.KMeans.classifier(observation, null, Distance);
            act.ShouldThrow<ArgumentNullException>();
        }

        [Theory]
        [InlineData(1d, 1d, 1d, 1)]
        [InlineData(2d, 2d, 2d, 2)]
        [InlineData(1d, 1d, 0d, 1)]
        [InlineData(0d, 0d, 1d, 1)]
        [InlineData(0d, 0d, 0d, 1)]
        [InlineData(3d, 3d, 3d, 2)]
        [InlineData(0d, 0d, 4d, 3)]
        public void ShouldClassifyObservationWithNearestCentroid(double x, double y, double z, int expected)
        {
            var observation = new[] {x,y,z};
            
            var centroid1 = new Tuple<int, double[]>(1, new[] {1d,1d,1d});
            var centroid2 = new Tuple<int, double[]>(2, new[] {2d,2d,2d});
            var centroid3 = new Tuple<int, double[]>(3, new[] {0d,0d,3d});

            var centroids = new [] { centroid1, centroid2, centroid3 };
            
            var result = StackOverflow.KMeans.classifier(observation, centroids, Distance);
            result.Should().Be(expected);
        }

    }
}

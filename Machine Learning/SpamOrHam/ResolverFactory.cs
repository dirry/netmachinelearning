using DryIoc;

namespace SpamOrHam
{
    public static class ResolverFactory
    {
        public static IResolver Create()
        {
            var container = new Container();

            container.Register<IApplication, Application>();
            container.Register<IStreamByLineReader, StreamByLineReader>();
            container.Register<ISpamFilterFactory, BayesSpamFilterFactory>();
            
            return container;
        }
    }
}
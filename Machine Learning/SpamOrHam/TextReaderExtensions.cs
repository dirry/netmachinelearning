using System;
using System.Reactive.Linq;

namespace SpamOrHam
{
    internal static class TextReaderExtensions
    {
        internal static IObservable<Message> ToMessages(this IObservable<string> lines)
        {
            return Observable.Create<Message>(observer =>
            {
                return lines.Subscribe(line =>
                {
                    var message = MapLineToMessage(line);
                    observer.OnNext(message);
                }, 
                observer.OnError,
                observer.OnCompleted);
            });
        }
        
        private static Message MapLineToMessage(string line)
        {
            var x = line.Split('\t');
            var spamOrHam = x[0] == "spam" ? SpamOrHam.Spam : SpamOrHam.Ham;
            var content = x[1];
            var message = new Message(spamOrHam, content);
            return message;
        }
    }
}
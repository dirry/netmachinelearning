﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Threading.Tasks;

namespace SpamOrHam
{
    public sealed class BayesSpamFilterFactory : ISpamFilterFactory
    {
        public async Task<ISpamFilter> CreateAsync(IObservable<Message> messages, Func<Task> readMessages)
        {
            var result = await GetStatisticsAndTestMessagesAsync(messages, readMessages);
            var statistics = result.Item1;
            var testMessages = result.Item2;

            var betaFinder = new BetaFinder(testMessages.ToObservable(), statistics);
            var beta = await betaFinder.GetBetaAsync();
            var confusion = await betaFinder.GetConfusionAsync();

            return new BayesSpamFilter(statistics, beta, confusion);
        }

        private static async Task<Tuple<Statistics,IList<Message>>> GetStatisticsAndTestMessagesAsync(IObservable<Message> messages, Func<Task> readMessages)
        {
            IObservable<Message> testMessagesOutput;
            var testMessagesTask = new TaskCompletionSource<bool>();
            var testMessages = new List<Message>();

            var learnData = messages.Split(out testMessagesOutput);

            using (var subscription = testMessagesOutput.Subscribe(testMessages.Add, () => testMessagesTask.SetResult(true)))
            {
                var statisticsTask = LearnMessagesToStatisticsAsync(learnData);

                await readMessages();
                await testMessagesTask.Task;

                var statistics = await statisticsTask;

                return new Tuple<Statistics, IList<Message>>(statistics, testMessages);
            }
        }

        private static async Task<Statistics> LearnMessagesToStatisticsAsync(IObservable<Message> learnMessages)
        {
            var getTokens = new TaskCompletionSource<Statistics>();
            var statistics = new Statistics();

            var tokens = learnMessages.ToTokens();
            var countTokens = tokens.Count().ToTask();

            using (var subscription = tokens.Subscribe(statistics.Add, () => getTokens.SetResult(statistics)))
            {
                if (await countTokens != (await getTokens.Task).TokenCount)
                    throw new InvalidOperationException();
            }

            return statistics;
        }
    }
}
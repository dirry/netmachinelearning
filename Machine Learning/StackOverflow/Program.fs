﻿open System
open StackOverflow.Charts
open StackOverflow.KMeans
open StackOverflow.Observations
open StackOverflow.InformationCriterions
open StackOverflow.PCA

open FSharp.Charting

open MathNet.Numerics.LinearAlgebra

[<EntryPoint>]
let main argv = 
    
    let scale (row:float[]) =
        let min = row |> Array.min
        let max = row |> Array.max
        if min = max
        then row
        else
            row |> Array.map (fun x -> (x - min) / (max - min))

    let test = observations.[..99] |> Array.map scale
    let train = observations.[100..] |> Array.map scale

    let similarity (row1:float[]) (row2:float[]) =
        1. / (1. + distance row1 row2)

    let split (row:float[]) =
        row.[..19],row.[20..]

    let weights (values:float[]) =
        let total = values |> Array.sum
        values
        |> Array.map (fun x -> x / total)


    let predict (row:float[]) =
        let known,unknown = row |> split
        let similarities = 
            train
            |> Array.map (fun example -> 
                let common, _ = example |> split 
                similarity known common)
            |> weights

        [| for i in 20..29 ->
            let column = train |> Array.map (fun x -> x.[i])
            let prediction = 
                (similarities, column)
                ||> Array.map2 (fun s v -> s*v)
                |> Array.sum
            prediction |] 

    let targetTags = headers.[20..]
    predict test.[0] 
    |> Array.zip targetTags
    |> Array.iter (fun (label, value) -> printfn "%f\t%s" (value*1.6) label)

    let validation =
        test
        |> Array.map (fun obs ->
            let actual = obs |> split |> snd
            let predicted = obs |> predict
            let recomended, observed =
                Array.zip predicted actual
                |> Array.maxBy fst
            if observed > 0. then 1. else 0.)
        |> Array.average
        |> printfn "Correct calls (with prediction): %f"

    let averages = [|
        for i in 20 .. 29 ->
        train |> Array.averageBy(fun row -> row.[i]) |]

    let baseline =
        test
        |> Array.map (fun obs ->
            let actual = obs |> split |> snd
            let predicted = averages
            let recommended, observed =
                Array.zip predicted actual
                |> Array.maxBy fst
            if observed > 0. then 1. else 0.)
        |> Array.average
        |> printfn "Correct calls (when random): %f"



    Console.ReadLine() |> ignore
    0 
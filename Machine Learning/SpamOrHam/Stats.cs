namespace SpamOrHam
{
    internal struct Stats
    {
        internal Stats(int hamCount, int spamCount)
        {
            HamCount = hamCount;
            SpamCount = spamCount;
        }

        internal int HamCount{ get;  }
        internal int SpamCount{ get; }

        internal int Count => HamCount + SpamCount;

        public static Stats operator +(Stats left, Stats right)
        {
            return new Stats(left.HamCount + right.HamCount, left.SpamCount + right.SpamCount);
        }
    }
}
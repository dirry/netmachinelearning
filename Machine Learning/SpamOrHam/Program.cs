﻿using DryIoc;

namespace SpamOrHam
{
    internal class Program
    {
        private const string Path = @"./Data/SMSSpamCollection.txt";

        private static void Main()
        {
            var resolver = ResolverFactory.Create();
            var app = resolver.Resolve<IApplication>();
            app.RunAsync(Path).Wait();
        }
    }
}

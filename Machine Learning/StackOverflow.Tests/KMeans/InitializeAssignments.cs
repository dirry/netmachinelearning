﻿using System;
using FluentAssertions;
using Xunit;

namespace StackOverflow.Tests.KMeans
{
    public sealed class InitializeAssignments
    {
        private static double[][] Observations => new[]
        {
            new[] { 05d, 06d, 07d, 08d, 09d, 10d },
            new[] { 08d, 09d, 10d, 11d, 12d, 13d },
            new[] { 13d, 14d, 15d, 16d, 17d, 18d },
            new[] { 21d, 22d, 23d, 24d, 25d, 26d },
            new[] { 34d, 35d, 36d, 37d, 38d, 39d }, 
            new[] { 55d, 56d, 57d, 58d, 59d, 60d }, 
            new[] { 89d, 90d, 91d, 92d, 93d, 94d } 
        };
        
        [Fact]
        public void ShouldNotThrow()
        {
            Action result = () => StackOverflow.KMeans.initializeAssignments(Observations);
            result.ShouldNotThrow();
        }
        
        [Fact]
        public void ShouldThrow_WhenObservationsAreNull()
        {
            Action result = () => StackOverflow.KMeans.initializeAssignments(null);
            result.ShouldThrow<ArgumentNullException>();
        }

        [Fact]
        public void ShouldReturnAssignments()
        {
            var assignments = StackOverflow.KMeans.initializeAssignments(Observations);
            
            assignments.Should().NotBeNull();
            assignments.Length.Should().Be(7);
            assignments[0].Item1.Should().Be(0);
            assignments[0].Item2[0].Should().Be(5d);
            assignments[1].Item1.Should().Be(0);
            assignments[1].Item2.Length.Should().Be(6);
            assignments[1].Item2[0].Should().Be(8d);
        }
    }
}

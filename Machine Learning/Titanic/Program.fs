﻿open FSharp.Data

type Titanic = CsvProvider<"titanic.csv">
type Passenger = Titanic.Row
let dataset = Titanic.GetSample()

[<EntryPoint>]
let main (argv:string[]):int = 
    
    dataset.Rows
    |> Seq.countBy (fun p -> p.Survived)
    |> Seq.iter (printfn "survived: %A")
    
    dataset.Rows
    |> Seq.averageBy (fun p -> if p.Survived then 1.0 else 0.0)
    |> printfn "Chances of survival: %.3f"

    let survivalRate (passengers:Passenger seq) = 
        let total = passengers |> Seq.length
        let survivors = 
            passengers
            |> Seq.filter (fun p -> p.Survived)
            |> Seq.length
        100.0 * (float survivors / float total)

    let bySex = 
        dataset.Rows
        |> Seq.groupBy (fun p -> p.Sex)

    bySex
    |> Seq.iter (fun (sex, passengersOfThisSex) -> printfn "Sex %A: %f" sex (survivalRate passengersOfThisSex))

    let byClass = 
        dataset.Rows
        |> Seq.sortBy (fun p -> p.Pclass)
        |> Seq.groupBy (fun p -> p.Pclass)

    byClass
    |> Seq.iter (fun (passengerClass, passengersOfThisClass) -> printfn "Class %A: %f" passengerClass (survivalRate passengersOfThisClass))

    let byClassAndSex = 
        dataset.Rows
        |> Seq.groupBy (fun p -> (p.Sex, p.Pclass))
        |> Seq.map (fun ((sex, clas), ps) -> ((sex, clas), (survivalRate ps)))
        |> Seq.sortBy (fun ((sex, clas), survRate) -> survRate)
    
    byClassAndSex
    |> Seq.iter (fun ((sex, clas), survRate) -> printfn "Sex %A Class %A Rate %f" sex clas survRate)

    let mostFrequentLabelIn group =
        group
        |> Seq.countBy snd
        |> Seq.maxBy snd
        |> fst

    let learn (extractFeature: Passenger -> string) (extractLabel: Passenger -> bool) (sample:Passenger seq):(Passenger -> bool) =
        let groups =
            sample
            |> Seq.map (fun obs -> extractFeature obs, extractLabel obs)
            |> Seq.groupBy fst
            |> Seq.map (fun (feat,group) -> feat, mostFrequentLabelIn group)

        let classifier obs =
            let featureValue = extractFeature obs
            groups
            |> Seq.find (fun (f, _) -> f = featureValue)
            |> snd

        classifier

    let survived (p:Passenger) = p.Survived
    let sex (p:Passenger):string = p.Sex

    let sexClassifier = 
        dataset.Rows
        |> learn sex survived

    let result = 
        dataset.Rows
        |> Seq.averageBy (fun p -> if p.Survived = sexClassifier p then 1.0 else 0.0)
    printfn "Stump: classify based on passenger sex: %.10f" result

    let classClassifier = 
        dataset.Rows
        |> learn (fun p -> string p.Pclass) survived
    
    let survivedPerClass = 
        dataset.Rows
        |> Seq.averageBy (fun p -> if p.Survived = classClassifier p then 1.0 else 0.0)

    printfn "Stump: classify based on passenger Class: %f" survivedPerClass

    let survivalByPricePaid =
        dataset.Rows
        |> Seq.sortBy (fun p -> p.Fare)
        |> Seq.groupBy (fun p -> p.Fare)
        |> Seq.iter (fun (price,passengers) ->
            printfn "%6.2F: %6.2f" price (survivalRate passengers))

    let averageFare =
        dataset.Rows
        |> Seq.averageBy (fun p -> p.Fare)

    let fareLevel (p:Passenger) =
        if p.Fare < averageFare
        then "Cheap"
        else "Expensive"

    let FareClassifier = 
        dataset.Rows 
        |> learn fareLevel survived

    let SurvivedByFareLevel =
        dataset.Rows
        |> Seq.averageBy (fun p ->
            if p.Survived = FareClassifier p then 1.0 else 0.0)

    printfn "Stump: classify based on fare level: %f" SurvivedByFareLevel

    let survivalByPortOfOrigin =
        dataset.Rows
        |> Seq.groupBy (fun p -> p.Embarked)
        |> Seq.iter (fun (port,passengers) -> printfn "%s: %f" port (survivalRate passengers))

    let hasData (extractFeature:(Passenger -> string option)):(Passenger -> bool) = 
        extractFeature >> Option.isSome

    let betterLearn (extractFeature:Passenger -> string option) (extractLabel:Passenger -> bool) (sample:Passenger seq): (Passenger -> bool) =
        let branches =
            sample
            |> Seq.filter (extractFeature |> hasData)
            |> Seq.map (fun obs -> extractFeature obs |> Option.get, extractLabel obs)
            |> Seq.groupBy fst
            |> Seq.map (fun (feat,group) -> feat, mostFrequentLabelIn group)
            |> Map.ofSeq

        let labelForMissingValues =
            sample
            |> Seq.countBy extractLabel
            |> Seq.maxBy snd
            |> fst

        let classifier (obs:Passenger):bool =
            let featureValue = extractFeature obs
            match featureValue with
            | None -> labelForMissingValues
            | Some(value) ->
                match (branches.TryFind value) with
                | None -> labelForMissingValues
                | Some(predictedLabel) -> predictedLabel 
        classifier
        
    let port (p:Passenger) : string option =
        if p.Embarked = "" then None
        else Some(p.Embarked)

    let updatedClassifier = 
        dataset.Rows
        |> betterLearn port survived

    let precision = 
        dataset.Rows
        |> Seq.averageBy (fun p ->
            if p.Survived = updatedClassifier p then 1.0 else 0.0)
    printfn "classify based on port of origin: %A" precision

    0 // return an integer exit code

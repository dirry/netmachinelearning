using System;
using System.Linq;

namespace ShadesOfGrey
{
    public sealed class ObservationFactory : IObservationFactory<byte>
    {
        public Observation<byte> Create(string data)
        {
            if (string.IsNullOrWhiteSpace(data))
                throw new ArgumentNullException(nameof(data));

            var values = data.Split(',');

            var label = values.First();
            var pixels = values.Skip(1).Select(v => Convert.ToByte(v));

            return new Observation<byte>(label, pixels);
        }
    }
}
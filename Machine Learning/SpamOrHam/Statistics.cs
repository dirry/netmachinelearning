using System.Collections.Generic;

namespace SpamOrHam
{
    internal class Statistics
    {
        internal int SpamCount { get; private set; }
        internal int HamCount { get; private set; }
        internal int TokenCount => SpamCount + HamCount;

        private readonly object _statisticsTableLock = new object();
        private IDictionary<string, Stats> StatisticsTable { get; } = new Dictionary<string,Stats>();
        internal Stats this[string token]
        {
            get
            {
                if (Contains(token))
                    return StatisticsTable[token];

                var halfOfTokenCount = TokenCount/2;
                return new Stats(halfOfTokenCount, halfOfTokenCount);
            }
        }

        internal bool Contains(string token)
        {
            return StatisticsTable.ContainsKey(token);
        }
        
        internal void Add(Token token)
        {
            var tokenStats = token.Type == SpamOrHam.Ham ? new Stats(1,0) : new Stats(0,1);

            lock (_statisticsTableLock)
            {
                if (StatisticsTable.ContainsKey(token.Value))
                {
                    var oldStats = StatisticsTable[token.Value];
                    var newStats = oldStats + tokenStats;
                    StatisticsTable[token.Value] = newStats;
                }
                else
                {
                    StatisticsTable.Add(token.Value, tokenStats);
                }
            }

            if (token.Type == SpamOrHam.Ham)
            {
                HamCount++;
            }
            else
            {
                SpamCount++;
            }
        }

        public override string ToString()
        {
            return string.Format($"Statistics: {TokenCount} Tokens ({SpamCount} Spams; {HamCount} Hams)");
        }
    }
}
﻿namespace SpamOrHam
{
    public interface ISpamFilter
    {
        Confusion Confusion { get; }

        bool IsPropablySpam(string message);
    }
}
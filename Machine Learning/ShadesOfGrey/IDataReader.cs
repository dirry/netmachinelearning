﻿using System;
using System.IO;

namespace ShadesOfGrey
{
    public interface IDataReader<T>
    {
        IObservable<Observation<T>> ReadObservations(FileSystemInfo dataPath);
    }
}
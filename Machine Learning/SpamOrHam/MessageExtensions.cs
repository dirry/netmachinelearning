using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using MoreLinq;

namespace SpamOrHam
{
    internal static class MessageExtensions
    {
        internal static IObservable<Message> Ham(this IObservable<Message> messages)
        {
            return messages.Where(m => m.Type == SpamOrHam.Ham);
        }

        internal static IObservable<Message> Spam(this IObservable<Message> messages)
        {
            return messages.Where(m => m.Type == SpamOrHam.Spam);
        }

        internal static IObservable<Token> ToTokens(this IObservable<Message> messages)
        {
            return Observable.Create<Token>(observer =>
            {
                return messages.Subscribe(message =>
                {
                    message.ToTokens().Distinct().ForEach(observer.OnNext);
                }, 
                observer.OnError, 
                observer.OnCompleted);
            });
        }

        internal static IEnumerable<Token> ToTokens(this Message message)
        {
            var type = message.Type;

            var tokens = message.Content.ToTokens();

            return tokens.Select(token => new Token(type, token));
        }

        internal static IEnumerable<string> ToTokens(this string message)
        {
            return message
                .Sanitize()
                .ToLower()
                .Split(' ')
                .Where(s => !string.IsNullOrWhiteSpace(s));
        }

        internal static string Sanitize(this string input)
        {
            return input
                .Replace('.', ' ')
                .Replace('!', ' ')
                .Replace('?', ' ')
                .Replace(',', ' ')
                .Replace(';', ' ')
                .Replace(':', ' ')
                .Replace('-', ' ')
                .Replace('(', ' ')
                .Replace(')', ' ')
                .Replace('\'', ' ')
                .Replace('"', ' ')
                .Replace('/', ' ')
                .Replace('\\', ' ')
                .Replace('$', ' ')
                .Replace('&', ' ')
                .Replace('#', ' ')
                .Replace('=', ' ')
                .Replace('*', ' ');
        }
    }
}
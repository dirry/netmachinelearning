using System;
using System.Collections.Generic;
using System.Linq;

namespace ShadesOfGrey
{
    public sealed class ManhattanDistance : IDistanceCalculator<byte>
    {
        public double Calculate(IList<byte> left, IList<byte> right)
        {
            if (left.Count != right.Count)
                throw new ArgumentException("Count of left collection and right collection do not match.");

            return left.Zip(right, (x, y) => (double)x - y)
                .Select(Math.Abs).Sum();
        }
    }
}



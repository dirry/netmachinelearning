﻿using FluentAssertions;
using Xunit;

namespace StackOverflow.Tests.Observation
{
    public sealed class Distance
    {
        [Theory]
        [InlineData(1d, 2d, 1d)]
        [InlineData(2d, 1d, 1d)]
        [InlineData(4d, 2d, 4d)]
        [InlineData(2d, 4d, 4d)]
        public void ShouldReturnDistance_For1DVectors(double a, double b, double expected)
        {
            var observation1 = new [] { a };
            var observation2 = new [] { b };

            var result = Observations.distance(observation1, observation2);
            result.Should().Be(expected);
        }
        
        [Theory]
        [InlineData(1d, 4d, 2d, 2d, 5d)]
        [InlineData(7d, 3d, 10d, 8d, 34d)]
        public void ShouldReturnDistance_For2DVectors(double a0, double a1, double b0, double b1, double expected)
        {
            var observation1 = new [] { a0, a1 };
            var observation2 = new [] { b0, b1 };

            var result = Observations.distance(observation1, observation2);
            result.Should().Be(expected);
        }
    }
}

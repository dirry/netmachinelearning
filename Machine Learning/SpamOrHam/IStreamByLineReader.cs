using System;
using System.IO;
using System.Threading.Tasks;

namespace SpamOrHam
{
    internal interface IStreamByLineReader: IObservable<string>
    {
        Task RunAsync(Func<Stream> streamFactory);
    }
}
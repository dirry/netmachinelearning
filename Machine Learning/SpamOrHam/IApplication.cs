﻿using System.Threading.Tasks;

namespace SpamOrHam
{
    internal interface IApplication
    {
        Task RunAsync(string path);
    }
}
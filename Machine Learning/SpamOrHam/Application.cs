using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace SpamOrHam
{
    internal sealed class Application : IApplication
    {
        private IStreamByLineReader StreamByLineReader { get; }
        private ISpamFilterFactory SpamFilterFactory { get; }

        public Application(IStreamByLineReader streamByLineReader, ISpamFilterFactory spamFilterFactory)
        {
            StreamByLineReader = streamByLineReader;
            SpamFilterFactory = spamFilterFactory;
        }

        public async Task RunAsync(string path)
        {
            var messages = StreamByLineReader.ToMessages();

            ISpamFilter spamFilter; 
            var timer = Stopwatch.StartNew();
            try
            {
                spamFilter = await SpamFilterFactory.CreateAsync(messages, () => StreamByLineReader.RunAsync(() => File.OpenRead(path)));
            }
            finally
            {
                timer.Stop();
            }
            Console.WriteLine("Required {0}ms", timer.Elapsed);

            OutputConfusionStatisticsToConsole(spamFilter.Confusion);
        }

        private static void OutputConfusionStatisticsToConsole(Confusion confusion)
        {
            Console.WriteLine("===========================================");
            Console.WriteLine("Confusion Matrix   Positive\tNegative");
            Console.WriteLine("Positive           TP: {0:p}\tFN: {1:p}", confusion.TruePositiveRatio,  confusion.FalseNegativeRatio);
            Console.WriteLine("Negative           FP: {0:p}\tTN: {1:p}", confusion.FalsePositiveRatio, confusion.TrueNegativeRatio);
            Console.WriteLine("===========================================");

            Console.WriteLine($"Accuracy: {confusion.Accuracy():p}");
            Console.WriteLine($"Precision: {confusion.Precision():p}");
            Console.WriteLine($"Recall: {confusion.Recall():p}");
            Console.WriteLine($"F-Measure: {confusion.FMeasure():p}");
        }
    }
}
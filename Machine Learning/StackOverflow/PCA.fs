﻿namespace StackOverflow

open MathNet
open MathNet.Numerics.LinearAlgebra
open MathNet.Numerics.Statistics
open StackOverflow.Observations
open FSharp.Charting

module PCA = 
    let correlations = 
        observations 
        |> Matrix.Build.DenseOfColumnArrays
        |> Matrix.toRowArrays
        |> Correlation.PearsonMatrix

    let correlated = 
        [
            for col in 0..(features-1) do
                for row in (col+1)..(features-1) -> 
                    correlations.[col, row], headers.[col], headers.[row]
        ]

        |> Seq.sortByDescending (fun (corr, _, _) -> abs corr)
        |> Seq.take 20
        |> Seq.iter (fun (corr, f1, f2) -> printfn "%s %s : %.2f" f1 f2 corr)

    let covarianceMatrix (M:Matrix<float>) =
        let cols = M.ColumnCount
        let C = DenseMatrix.create cols cols Matrix.Zero
        for c1 in 0 .. (cols - 1) do
            C.[c1,c1] <- Statistics.Variance (M.Column c1)
            for c2 in (c1 + 1) .. (cols - 1) do
                let cov = Statistics.Covariance (M.Column c1, M.Column c2)
                C.[c1,c2] <- cov
                C.[c2,c1] <- cov
        C

    let normalize dim (observations:float[][]) = 
        let averages:float[] = Array.init dim (fun i -> observations |> Seq.averageBy(fun x -> x.[i]))
        let stdDevs:float[] = Array.init dim (fun i -> 
                                      let avg = averages.[i]
                                      observations 
                                      |> Seq.averageBy (fun x -> pown (float x.[i] - avg) 2 |> sqrt))

        observations 
        |> Array.map (fun row -> row |> Array.mapi (fun i x -> (float x - averages.[i]) / stdDevs.[i]))

    let pca (observations:float[][]) = 
        let factorization = 
            observations
            |> Matrix.Build.DenseOfRowArrays
            |> covarianceMatrix 
            |> Matrix.eigen 

        let eigenValues = factorization.EigenValues
        let eigenVectors = factorization.EigenVectors

        let projector (obs:float[]) = 
            let obsVector = obs |> Vector.Build.DenseOfArray
            (eigenVectors.Transpose () * obsVector)
            |> Vector.toArray

        eigenValues, eigenVectors, projector

    let printFeatures = 
        let normalized = normalize (headers.Length) observations
        let eValues, eVectors, projector = pca normalized
        let total = eValues |> Seq.sumBy (fun x -> x.Magnitude)
        eValues
        |> Vector.toList
        |> List.rev
        |> List.scan (fun (percent, cumul) value -> let percent = 100. * value.Magnitude / total
                                                    let cumul = cumul + percent
                                                    (percent, cumul)) (0.,0.)
        |> List.tail
        |> List.iteri (fun i (p,c) -> printfn "Feature %2i: %.2f%% (%.2f%%)" i p c)

    //showprincipalComponents 1 2 
    let showPrincipalComponents comp1 comp2 = 
        let normalized = normalize (headers.Length) observations
        let eValues, eVectors, projector = pca normalized
        let title = sprintf "Component %i vs %i" comp1 comp2
        let features = headers.Length
        let coords = Seq.zip (eVectors.Column (features-comp1)) (eVectors.Column (features-comp2))

        Chart.Point (coords, Title=title, Labels = headers, MarkerSize = 7)
        |> Chart.WithXAxis(
            Min = -1.0, Max=1.0, 
            MajorGrid = ChartTypes.Grid(Interval = 0.25), 
            LabelStyle = ChartTypes.LabelStyle(Interval = 0.25), 
            MajorTickMark = ChartTypes.TickMark(Enabled = false))
        |> Chart.WithYAxis(
            Min = -1.0, Max=1.0, 
            MajorGrid = ChartTypes.Grid(Interval = 0.25), 
            LabelStyle = ChartTypes.LabelStyle(Interval = 0.25), 
            MajorTickMark = ChartTypes.TickMark(Enabled = false))
        |> Chart.Show

    //showProjections 1 2 
    let showProjections comp1 comp2 = 
        let normalized = normalize (headers.Length) observations
        let eValues, eVectors, projector = pca normalized
        let title = sprintf "Component %i vs %i" comp1 comp2
        let features = headers.Length
        let coords = 
            normalized 
            |> Seq.map projector
            |> Seq.map (fun obs -> obs.[features-comp1], obs.[features-comp2])
        Chart.Point (coords, Title = title)
        |> Chart.WithXAxis(
            Min = -200.0, Max=500.0, 
            MajorGrid = ChartTypes.Grid(Interval = 100.), 
            LabelStyle = ChartTypes.LabelStyle(Interval = 100.), 
            MajorTickMark = ChartTypes.TickMark(Enabled = false))
        |> Chart.WithYAxis(
            Min = -200.0, Max=500.0, 
            MajorGrid = ChartTypes.Grid(Interval = 100.), 
            LabelStyle = ChartTypes.LabelStyle(Interval = 100.), 
            MajorTickMark = ChartTypes.TickMark(Enabled = false))
        |> Chart.Show
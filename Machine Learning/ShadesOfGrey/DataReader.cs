using System;
using System.IO;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ShadesOfGrey
{
    public sealed class DataReader<T> : IDataReader<T>
    {
        public DataReader(IObservationFactory<T> observationFactory)
        {
            ObservationFactory = observationFactory;
        }

        private IObservationFactory<T> ObservationFactory { get; }

        public IObservable<Observation<T>> ReadObservations(FileSystemInfo dataPath)
        {
            return Observable.Create<Observation<T>>(
                async(observer,token) => await ReadObservationsAsync(dataPath, observer, token));
        }

        private async Task<IDisposable> ReadObservationsAsync(FileSystemInfo dataPath, IObserver<Observation<T>> observer, CancellationToken token)
        {
            using (var fileStream = File.OpenRead(dataPath.FullName))
            using (var streamReader = new StreamReader(fileStream))
            {
                await SkipFirstLineAsync(streamReader);

                while (!streamReader.EndOfStream)
                {
                    if (!token.IsCancellationRequested)
                    {
                        var observation = await ReadNextObservationAsync(streamReader);
                        observer.OnNext(observation);
                    }
                    else
                    {
                        observer.OnError(new OperationCanceledException(token));
                        return Disposable.Empty;
                    }
                }
            }

            observer.OnCompleted();
            return Disposable.Empty;
        }

        private static async Task SkipFirstLineAsync(TextReader streamReader)
        {
            await streamReader.ReadLineAsync();
        }

        private async Task<Observation<T>> ReadNextObservationAsync(TextReader streamReader)
        {
            var line = await streamReader.ReadLineAsync();
            return ObservationFactory.Create(line);
        }
    }
}
﻿namespace SpamOrHam
{
    public sealed class Confusion
    {
        public double TruePositive { get; }
        public double FalseNegative { get; }
        public double FalsePositive { get; }
        public double TrueNegative { get; }

        public double TruePositiveRatio => TruePositive / TotalPopulation;
        public double FalseNegativeRatio => FalseNegative / TotalPopulation;
        public double FalsePositiveRatio => FalsePositive / TotalPopulation;
        public double TrueNegativeRatio => TrueNegative / TotalPopulation;

        public double ConditionPositive => TruePositive + FalseNegative;
        public double ConditionNegative => FalsePositive + TrueNegative;
        public double TotalPopulation => ConditionPositive + ConditionNegative;
        public double TestOutcomePositive => TruePositive + FalsePositive;
        public double TestOutcomeNegative => FalseNegative + TrueNegative;

        public Confusion (double truePositive, double falseNegative, double falsePositive, double trueNegative)
        {
            TruePositive = truePositive;
            FalseNegative = falseNegative;
            FalsePositive = falsePositive;
            TrueNegative = trueNegative;
        }
    }
}

